'use strict';

eventsApp.factory('eventData', function ($resource) {
    var resource = $resource('/data/events/:id', {id:'@id'});
    return {
        getEvent: function(eventId) {
            return resource.get({id:eventId});
        },
        save: function(event) {
            event.id = 9999;
            return resource.save(event);
        },
        getAllEvents: function() {
            return resource.query();
        }
    }
});