'use strict';

eventsApp.directive('dateKeys', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs, controller ){
            element.on('keydown', function(event){
                if(event.keyCode==64)
                    return true;
                return false;
            });
        }
    };
});