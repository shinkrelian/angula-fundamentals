'use strict';

eventsApp.directive('eventThumbnail', function(){
    return {
        replace: true,
        restrict: 'E',
        templateUrl: "/templates/directives/eventThumbnail.html",
        scope: {
            event: "="
        }
    };
});