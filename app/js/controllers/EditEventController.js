'use strict';

eventsApp.controller('EditEventController',
    function($scope, eventData,  $log){
        $scope.saveEvent = function(event, newEventForm){
            if (newEventForm.$valid){
                eventData.save(event)
                    .$promise
                    .then(function(response){ $log.log('success', response); window.location = "/EventDetails.html"; })
                    .catch(function(response){ $log.log('failure', response); });
             }
        };
        $scope.cancelEdit = function(){
            window.location = "/EventDetails.html"
        };
    }
);