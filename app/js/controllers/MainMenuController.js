'use strict';

eventsApp.controller('MainMenuController',
    function($scope, $location){
        $scope.createEvent = function(){
            $location.replace();
            $location.url('/newEvent');
        };
    }
);