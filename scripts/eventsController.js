var fs = require('fs');
var path = 'app/data/event/';

module.exports.get = function(req, res) {
    console.log('get', req);
    var event = fs.readFileSync(path + req.params.id + '.json', 'utf8');
    res.setHeader('Content-Type', 'application/json');
    res.send(event);
}

module.exports.getAll = function(req, res) {
    console.log('getAll', req);
    var files = [];
    try{
        files = fs.readdirSync(path);
    }
    catch(e){
        res.send([]);
        res.end();
    }
    var results = "[";
    for(var idx=0; idx<files.length; idx++){
        if (files[idx].indexOf('.json') == files[idx].length-5){
            results += fs.readFileSync(path + files[idx]) + ",";
        }
    }
    results = results.substr(0, results.length-1);
    results += "]";

    res.setHeader('Content-Type', 'application/json');
    res.send(results);
    res.end();
}

module.exports.save = function(req, res) {
    console.log('save', req.body);
    var event = req.body;
    fs.writeFileSync(path + event.id + '.json', JSON.stringify(event));
    res.send(event);
}