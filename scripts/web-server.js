var express = require('express');
var path = require('path');
var events = require('./eventsController');
var app = express();
var rootPath =  path.normalize(__dirname + '/../')
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static(rootPath + '/app'));

app.get('/data/events/:id', events.get);
app.get('/data/events', events.getAll);
app.post('/data/events/:id', events.save);
app.get('*', function(req, res){ res.sendFile(rootPath + '/app/Index.html'); });


app.listen(8000);
console.log('Listening on port 8000');